﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ImageUploadWithExif
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // Læs exif serverside
            var file = FileUpload1.PostedFile;
            var parser = new photo.exif.Parser();
            var exif = parser.Parse(file.InputStream);

            Literal1.Text = "";
            foreach (var tag in exif)
            {
                Literal1.Text += tag.Title + " = " + tag.Value + "<br />";
            }

            // Læs client variabler
            var gps = new { lat = hidLat.Value, lon = hidLon.Value };
        }
    }
}