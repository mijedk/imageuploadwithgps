﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ImageUploadWithExif.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/exif-js"></script>
    <script type="text/javascript" src="binaryfile.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" onClientChange="getExif()" />
        <asp:Button ID="Button1" runat="server" Text=".NET Exif" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" Text="JS Exif" OnClientClick="getExif(); return false;" UseSubmitBehavior="false" />

        <asp:Panel ID="Panel1" runat="server">
            <asp:Literal ID="Literal1" runat="server" />
        </asp:Panel>
    </div>
    </form>
    
    <script type="text/javascript">
        function getExif() {
            var file = document.getElementById("<%=FileUpload1.ClientID%>").files[0];
            var fr = new FileReader();
            fr.onloadend = function () {
                console.log(fr);
                var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(fr.result));

                document.getElementById("<%=Panel1.ClientID%>").innerHTML = "<pre>" + JSON.stringify(exif, null, 2) + "</pre>"; //.replace('\n', '<br />');

                var lat = EXIF.getTag(exif, "GPSLatitude");
                var lon = EXIF.getTag(exif, "GPSLongitude");

                console.log('lat', lat);
                console.log('lon', lon);


                if (typeof lat === 'undefined' || typeof lon === 'undefined')
                {
                    if (navigator.geolocation)
                    {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            document.getElementById("<%=Panel1.ClientID%>").innerHTML = "<pre>" + JSON.stringify(position, null, 2) + "</pre>"; //.replace('\n', '<br />');
                        });
                    }
                }
            }

            var t = fr.readAsDataURL(file);
            console.log(t);
        }

        function base64ToArrayBuffer(base64) {
            base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
            var binaryString = atob(base64);
            var len = binaryString.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = binaryString.charCodeAt(i);
            }
            return bytes.buffer;
        }
    </script>
</body>
</html>
