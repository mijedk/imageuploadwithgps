﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ImageUploadWithExif.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/exif-js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:FileUpload ID="FileUpload1" runat="server" onChange="getExif()" />
        <asp:HiddenField ID="hidLat" runat="server" />
        <asp:HiddenField ID="hidLon" runat="server" />
        <asp:HiddenField ID="hidGpsObtainedFrom" runat="server" />
        
        <asp:Button ID="Button1" runat="server" Text="Upload" OnClick="Button1_Click" />

        <asp:Panel ID="Panel1" runat="server">
            <asp:Literal ID="Literal1" runat="server" />
        </asp:Panel>
    </div>
    </form>
    
    <script type="text/javascript">
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1;

        function getExif() {
            document.getElementById("<%=hidLat.ClientID%>").value = ""; //position.coords.latitude;
            document.getElementById("<%=hidLon.ClientID%>").value = ""; //position.coords.longitude;
            document.getElementById("<%=hidGpsObtainedFrom.ClientID%>").value = "";

            if (isAndroid) {
                readLocationFromBrowser();
            } else {
                var file = document.getElementById("<%=FileUpload1.ClientID%>").files[0];
                var fr = new FileReader();
                fr.onloadend = readExif;

                var t = fr.readAsDataURL(file);
            }
        }

        function readLocationFromBrowser() {
            // Hent lokation via browseren
            if (navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(function (position)
                {
                    document.getElementById("<%=hidLat.ClientID%>").value = position.coords.latitude;
                    document.getElementById("<%=hidLon.ClientID%>").value = position.coords.longitude;
                    document.getElementById("<%=hidGpsObtainedFrom.ClientID%>").value = "browser";
                    //document.getElementById("<%=Panel1.ClientID%>").innerHTML = "<pre>" + position.coords.latitude + "\r\n" + position.coords.longitude + "</pre>"; //.replace('\n', '<br />');
                });
            } else {
                // TODO håndter fejl
                alert('location denied');
            }
        }

        function readExif() {
                var exif = EXIF.readFromBinaryFile(base64ToArrayBuffer(this.result));

                document.getElementById("<%=Panel1.ClientID%>").innerHTML = "<pre>" + JSON.stringify(exif, null, 2) + "</pre>"; //.replace('\n', '<br />');

                var lat = exif.GPSLatitude; // EXIF.getTag(exif, "GPSLatitude");
                var lon = exif.GPSLongitude; // EXIF.getTag(exif, "GPSLongitude");


                if (typeof lat === 'undefined' || typeof lon === 'undefined')
                {
                    readLocationFromBrowser();
                } else {
                    document.getElementById("<%=hidLat.ClientID%>").value = lat[0] + (lat[1] / 60) + (lat[2] / 60);
                    document.getElementById("<%=hidLon.ClientID%>").value = lon[0] + (lon[1] / 60) + (lon[2] / 60);;
                    document.getElementById("<%=hidGpsObtainedFrom.ClientID%>").value = "exif";
                }
            }

        function base64ToArrayBuffer(base64) {
            base64 = base64.replace(/^data\:([^\;]+)\;base64,/gmi, '');
            var binaryString = atob(base64);
            var len = binaryString.length;
            var bytes = new Uint8Array(len);
            for (var i = 0; i < len; i++) {
                bytes[i] = binaryString.charCodeAt(i);
            }
            return bytes.buffer;
        }
    </script>
</body>
</html>
